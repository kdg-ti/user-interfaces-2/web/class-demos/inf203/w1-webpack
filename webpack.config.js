import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin"

const config = {
  devtool:"source-map",
  mode: "development",
  plugins:[
    new HtmlWebpackPlugin({template: "./src/html/index.html"}),
    new MiniCssExtractPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        // voor het verschil kan je hier kijken https://maxrozen.com/difference-between-style-loader-mini-css-extract-plugin
        // use: ["style-loader", "css-loader"] // dit is soms makkelijker voor development
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.html?$/i,
        use: ['html-loader']
      },
      // Image assets
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        type: "asset"
      },
      // Font assets
      {
        test: /\.(woff2?|eot|ttf|otf)$/i,
        type: "asset"
      },
    ]
  },
  devServer:{
    static: {
      directory: path.resolve("dist")
    },
    open:true,
    hot:false
  }
}
export default config;
