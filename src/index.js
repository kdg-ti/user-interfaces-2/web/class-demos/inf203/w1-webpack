import 'bootstrap';
//import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootswatch/dist/sketchy/bootstrap.min.css'

import surface, {oppervlakCirkel} from "./js/geometry.js"
import "./css/styles.css";

function toonGegevens() {
  
  let size = input.value;
  let omtrek = document.getElementById("omtrek-cirkel");
  let oppervlak = document.getElementById("oppervlak-cirkel");
  
  if (size && !isNaN(size)) {
    omtrek.textContent = surface(size).toFixed(4)
    oppervlak.textContent = oppervlakCirkel(size).toFixed(2)
  }
  else {
    omtrek.textContent = ""
    oppervlak.textContent = ""
  }
}

const input = document.getElementById("size");
input.addEventListener("blur", toonGegevens)

